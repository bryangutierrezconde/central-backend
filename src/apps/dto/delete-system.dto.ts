/* eslint-disable prettier/prettier */
import { ApiProperty } from "@nestjs/swagger";
import { Matches } from "class-validator";
export class DeleteAppDto {
 
  @ApiProperty()
  id:string[];
}
