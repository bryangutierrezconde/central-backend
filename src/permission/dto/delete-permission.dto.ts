/* eslint-disable prettier/prettier */
import { ApiProperty } from "@nestjs/swagger";

export class DeletePermissionDto { 
  @ApiProperty()
  permissionId:string[]
}
